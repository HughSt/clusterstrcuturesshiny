library(raster)
library(sp)
library(leaflet)
library(RANN)
library(rgeos)
#hnd_adm0<-getData("GADM", country="HND",level=0)

get_clusters <- function(coords, dist_km=5){
  dist = dist_km/ 111 
  nearest <- nn2(coords,coords, eps=0.1,searchtype="radius", radius=dist, k=100)
  nn <- nearest$nn.idx
  
  left <- 1:nrow(coords)
  clusters<-list()
  cluster_point_ids<-list()
  i<-1
  
  while(length(left)>0){
    got_already <- left[1]
    row <- left[1]
    within_dist <- nn[row,which(nn[row,]>0)]
    while(length(within_dist)>0){
      not_checked <- within_dist[which(!(within_dist %in% got_already))]
      
      # Now you've checked that row, add it to the got_already vector
      got_already <- unique(c(got_already, within_dist))
      
      # go to the not checked rows and find any new ids
      within_dist  <- unique(as.vector(nn[not_checked,]))
      
      # remove the 0
      within_dist  <- within_dist [-which(within_dist ==0)]
    }
    
    clusters[[i]] <- coords[got_already,]
    cluster_point_ids[[i]] <- got_already
    i <- i+1
    left <- left[-which(left %in% unlist(cluster_point_ids))]
  }
  
  # loop through each cluster of points and create a unique buffer polygon
  PolyBuffer <- list()
  for(j in 1:length(clusters)){
    if(length(clusters[[j]])==2){
      points_sp <- SpatialPoints(matrix(clusters[[j]],nrow=1))
    }else{
      points_sp <- SpatialPoints(clusters[[j]])
    }
    PolyBuffer[[j]] <- gBuffer(points_sp, width=dist/2, id=j)
  }
  
  joined = SpatialPolygons(lapply(PolyBuffer, function(x){x@polygons[[1]]}), proj4string = crs(StructurePoints))
  joined$NumStructures = sapply(clusters, length)/2 # doesn't like nrow so using length/2
  joined$ClusterID = 1:length(clusters)
  SelectedStructures = StructurePoints@coords[!is.na(over(StructurePoints, joined)$ClusterID),]
  
  return(list(clusters = clusters, cluster_polygons = joined, selected_structures = SelectedStructures))
}

map <- leaflet() %>%
  addTiles("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", group = "OSM",
           attribution = "Powered by Open Street Maps & Google Earth Engine") %>%
  addLayersControl(overlayGroups = c("Structures", "Clusters"), options=layersControlOptions(collapsed = FALSE))

shinyServer(function(input, output){
  
  output$myMap <- renderLeaflet({
    
  inFile<-input$File
  #predFile<-input$PredFile
  if (is.null(inFile))
    return(NULL)
  #if (is.null(predFile))
  # return(NULL)
  
  points<-read.csv(inFile$datapath)
  StructurePoints<<-SpatialPoints(cbind(points$lng,points$lat),
                                 crs(" +proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))

    
  # 
  # cluster points
  withProgress(message = 'Loading app',
               detail = 'Hang on...', value = 0, {
                 
  Clusters<-reactive({get_clusters(coordinates(StructurePoints), dist_km=input$buffer)})
  print(head(Clusters()$cluster_polygons))
  
# map

 map %>%
    addCircleMarkers(coordinates(StructurePoints)[,1],
                     coordinates(StructurePoints)[,2],radius=1, group="Structures") %>%
   addPolygons(data=Clusters()$cluster_polygons, weight=1, opacity=0.5, fillOpacity=0.5, color="red",
               group="Clusters", popup = paste0("<strong>Cluster ID: </strong>",Clusters()$cluster_polygons$ClusterID,
                                             "<br>","<strong>Number of structures: </strong>",Clusters()$cluster_polygons$NumStructures))
   })
  })
  
 
#   observe({
  # withProgress(message = 'Loading app',
  #              detail = 'This may take a while...', value = 0, {
#                    leafletProxy("myMap") %>%
#                      clearShapes() %>%
#                      addPolygons(data=Clusters()$cluster_polygons, weight=1, opacity=0.5, fillOpacity=0.5,
#                                  group="Cases", popup = paste0("<strong>Cluster ID: </strong>",Clusters()$cluster_polygons$ClusterID,
#                                                                "<br>","<strong>Number of structures: </strong>",Clusters()$cluster_polygons$NumStructures))
# 
# 
#   })
# })
  
  output$logo <- renderImage({
    # When input$n is 1, filename is ./images/image1.jpeg
    filename <- normalizePath(file.path("logo3_transparent.png"))
    
    # Return a list containing the filename
    list(src = filename)
  }, deleteFile = FALSE)
  
  #output$Instructions <- textOutput("File with 'lng' and 'lat' columns")
  
})
